
main();

function main() {
  const canvas = document.querySelector('#glcanvas');
  const gl = canvas.getContext('webgl');

  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  const shaderProgramFactory = new ShaderProgramFactory(gl);
  const shaderProgram = shaderProgramFactory.createShaderProgram();

  const programInfo = {
    program: shaderProgram,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
      vertexNormal: gl.getAttribLocation(shaderProgram, 'aVertexNormal'),
      textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
      normalMatrix: gl.getUniformLocation(shaderProgram, 'uNormalMatrix'),
      uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
    },
  };

  const cubeBuffer = new CubeBuffer(gl);
  const buffers = cubeBuffer.startBuffer();

  const imageLoader = new ImageLoader(gl);
  const texture = imageLoader.loadImageIntoTexture( 'cubetexture.png');

  const repeatRender = new RepeatRender(gl);
  repeatRender.renderImage(programInfo, buffers, texture)

}
