
class RepeatRender {
    constructor(gl) {
        this.gl = gl;
    }

    renderImage(programInfo, buffers, texture) {
        const gl = this.gl;
        var then = 0;

        function render(now) {
            now *= 0.001;  // convert to seconds
            const deltaTime = now - then;
            then = now;

            drawScene(gl, programInfo, buffers, texture, deltaTime);

            requestAnimationFrame(render);
        }

        requestAnimationFrame(render);
    }
}
