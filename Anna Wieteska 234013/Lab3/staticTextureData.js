class StaticTextureData {
    constructor(texture, format, srcType) {
        this.texture = texture;
        this.level = 0;
        this.internalFormat = format;
        this.srcFormat = format;
        this.srcType = srcType;
    }
}