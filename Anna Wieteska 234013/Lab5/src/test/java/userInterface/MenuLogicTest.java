package userInterface;

import operations.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Scanner;

import static junit.framework.TestCase.assertEquals;

public class MenuLogicTest {
    private MenuLogic menuLogic;

    @Before
    public void init() {
        this.menuLogic = new MenuLogic();
    }

    @Test
    public void shouldSetProperNumberOneValue() {
        menuLogic.setNumberOne(5.0);

        assertEquals(5.0, menuLogic.getNumberOne());
    }

    @Test
    public void shouldSetProperNumberTwoValue() {
        menuLogic.setNumberTwo(5.0);

        assertEquals(5.0, menuLogic.getNumberTwo());
    }

    @Test
    public void shouldSetProperStrategy() {
        OperationStrategy strategy = new Addition();

        menuLogic.setStrategy(strategy);

        assertEquals(strategy, menuLogic.getStrategy());
    }

    @Test
    public void shouldSetProperResult() {
        menuLogic.setResult(10.0);

        assertEquals(10.0, menuLogic.getResult());
    }

    @Test
    public void shouldReturnAdditionStrategy(){
        menuLogic.setupStrategy(1);

        assertEquals(Addition.class, menuLogic.getStrategy().getClass());
    }

    @Test
    public void shouldReturnSubtractionStrategy(){
        menuLogic.setupStrategy(2);

        assertEquals(Subtraction.class, menuLogic.getStrategy().getClass());
    }

    @Test
    public void shouldReturnMultiplicationStrategy(){
        menuLogic.setupStrategy(3);

        assertEquals(Multiplication.class, menuLogic.getStrategy().getClass());
    }

    @Test
    public void shouldReturnDivisionStrategy(){
        menuLogic.setupStrategy(4);

        assertEquals(Division.class, menuLogic.getStrategy().getClass());
    }


}
