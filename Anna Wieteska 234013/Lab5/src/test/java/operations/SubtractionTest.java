package operations;

import exceptions.DivisionByZeroException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SubtractionTest {

    private OperationStrategy strategy;

    @Before
    public void init(){
        this.strategy = new Subtraction();
    }

    @Test
    public void shouldReturnTrueIfProperResultOfSubtraction() throws DivisionByZeroException {
        assertEquals(0.0,strategy.count(5,5));
    }

}