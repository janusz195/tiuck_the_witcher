package userInterface;

public class MenuInterface {

    public void printGreeting() {
        System.out.println("|||||||||||||||||||||||");
        System.out.println("|||Simple Calculator|||");
        System.out.println("|||||||||||||||||||||||");
    }

    public void printInstruction() {
        System.out.println("||||||INSTRUCTION||||||");
        System.out.println("|||||||||||||||||||||||");
        System.out.println("|||||Enter numbers|||||");
    }

    public void printEnteredValues(double firstNumber, double secondNumber) {
        System.out.println("|||||||||||||||||||||||");
        System.out.println("|||||Entered Values||||");
        System.out.println("First: " + firstNumber);
        System.out.println("Second: " + secondNumber);
        System.out.println("|||||||||||||||||||||||");
    }

    public void printSelectOperation() {
        System.out.println("||||Select operation|||");
        System.out.println("|||||||||||||||||||||||");
        System.out.println("||||| 1 - Addition|||||");
        System.out.println("|| 2 for Subtraction|||");
        System.out.println("| 3 for Multiplication|");
        System.out.println("|||| 4 for Division||||");
    }

    public void printResult(double result) {
        System.out.println("|||||||||||||||||||||||");
        System.out.println("||||||| RESULT ||||||||");
        System.out.println("|||||||||||||||||||||||");
        System.out.println(result);
    }

    public void printFarewell() {
        System.out.println("Thanks for using Calculator!");
    }

}
