package operations;

import exceptions.DivisionByZeroException;

public interface OperationStrategy {

    public double count(double firstNumber, double secondNumber) throws DivisionByZeroException;
}
