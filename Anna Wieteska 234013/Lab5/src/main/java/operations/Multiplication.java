package operations;

public class Multiplication implements OperationStrategy {
    @Override
    public double count(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }
}
