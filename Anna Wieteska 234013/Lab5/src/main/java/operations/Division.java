package operations;

import exceptions.DivisionByZeroException;

public class Division implements OperationStrategy {
    @Override
    public double count(double firstNumber, double secondNumber) throws DivisionByZeroException {
        if (secondNumber == 0) {
            throw new DivisionByZeroException();
        }

        return firstNumber / secondNumber;
    }
}
