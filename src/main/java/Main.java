import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    /**
     * Main method executed by program
     *
     * @param args table of string contains params for execution of program
     */
    public static void main(String[] args) {

        List<PrintCurrentTime> implementation = new ArrayList<>();
        implementation.add(new LukaszPrintCurrentTimeImpl());
        implementation.add(new Ania_PrintCurrentTime());
        implementation.add(new MagdalenaPrintCurrentTimeImpl());
        implementation.add(new MarcelinaPrintCurrentTimeImpl());
        if (args.length > 0) {
            try {
                int number = Integer.parseInt(args[0]);
                if (!implementation.isEmpty()) {
                    if (number - 1 < implementation.size()) {
                        PrintCurrentTime printer = implementation.get(number - 1);
                        System.out.println(printer.print());
                    } else {
                        System.out.println("No implementation in list");
                    }
                } else {
                    System.out.println("No implementations");
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                System.out.println("Wrong input parameters");
            }
        } else {
            System.out.println("None input parameters");
        }
    }
}
