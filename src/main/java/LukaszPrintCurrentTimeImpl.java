import java.time.LocalDateTime;

public class LukaszPrintCurrentTimeImpl implements PrintCurrentTime {
    @Override
    public String print() {
        return LocalDateTime.now().toString();
    }
}
