/**
 * class responsible for representation point objects
 */
public class Point {

    /**
     * represent value of X point on surface
     */
    private int x;
    /**
     * represent value of Y point on surface
     */
    private int y;

    public Point(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * @param source
     *            started point for calculate distance between points
     * @param destination
     *            second point for calculate distance between points
     * @return distance point as double
     */
    public static double calculateDistance(final Point source, final Point destination) {
        return Math.sqrt(
                Math.pow((destination.x - source.x), 2) + Math.pow((destination.y - source.y), 2));
    }
}
