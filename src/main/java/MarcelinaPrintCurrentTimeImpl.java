import java.util.Date;
import java.text.SimpleDateFormat;

public class MarcelinaPrintCurrentTimeImpl implements PrintCurrentTime {
    @Override
    public String print(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
}