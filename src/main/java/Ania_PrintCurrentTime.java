import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Ania_PrintCurrentTime implements PrintCurrentTime {
    @Override
    public String print(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
}