import java.time.ZonedDateTime;

public class MagdalenaPrintCurrentTimeImpl implements PrintCurrentTime {
    @Override
    public String print() {
        ZonedDateTime dateTime1 = ZonedDateTime.now( );
        return dateTime1.toString();
    }
}
