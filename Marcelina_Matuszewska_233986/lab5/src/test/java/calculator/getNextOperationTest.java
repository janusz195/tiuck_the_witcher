package calculator;

import org.junit.Test;

import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class getNextOperationTest {
    @Test
    public void askForNextOperation() {
        getNextOperation getNextO = new getNextOperation();
        final String yN = "y";
        InputStream stream = new ByteArrayInputStream(yN.getBytes());
        assertEquals('y', getNextO.askForNextOperation(stream));
    }
}