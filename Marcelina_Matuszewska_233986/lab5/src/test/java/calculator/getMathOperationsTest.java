package calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class getMathOperationsTest {

    @Test
    public void mathOperationsTest() {
        getMathOperations getMath = new getMathOperations();
        double delta = 0.000001;
        assertEquals(20, getMath.mathOperations(1, 10.0, 10.0), delta);
        assertEquals(0, getMath.mathOperations(2, 10.0, 10.0), delta);
        assertEquals(1, getMath.mathOperations(3, 10.0, 10.0), delta);
        assertEquals(100, getMath.mathOperations(4, 10.0, 10.0), delta);
        assertEquals(0, getMath.mathOperations(5, 10.0, 10.0), delta);
    }
}