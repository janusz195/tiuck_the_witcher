package calculator;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.*;

public class getValuesTest {

    @Test
    public void setValuesTest() {
        getValues getVal = new getValues();
        double delta = 0.0000001;
        final String value = "5";
        InputStream stream = new ByteArrayInputStream(value.getBytes());
        assertEquals(5, getVal.setValues(stream), delta);
    }
}