package calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class AdditionTest {
    Addition add = new Addition();
    @org.junit.Test
    public void NormalAdditionTest() {
        double delta = 0.0000001;
        assertEquals(2, add.count(1, 1), delta);
    }
    @org.junit.Test
    public void ZeroXZeroTest() {
        double delta = 0.0000001;
        assertEquals(0, add.count(0, 0), delta);
    }
    @org.junit.Test
    public void NumberXZeroTest() {
        double delta = 0.0000001;
        assertEquals(5, add.count(5, 0), delta);
    }
    @org.junit.Test
    public void ZeroXNumberTest() {
        double delta = 0.0000001;
        assertEquals(6, add.count(0, 6), delta);
    }
}