package calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class DivisionTest {
    Division div = new Division();
    @org.junit.Test
    public void NormalDivisionTest() {
        double delta = 0.0000001;
        assertEquals(1, div.count(1, 1), delta);
    }
    @org.junit.Test
    public void ZeroXNumberTest() {
        double delta = 0.0000001;
        assertEquals(0, div.count(0, 5), delta);
    }
}