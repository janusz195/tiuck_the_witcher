package calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class SubtractionTest {
    Subtraction sub = new Subtraction();
    @org.junit.Test
    public void NormalAdditionTest() {
        double delta = 0.0000001;
        assertEquals(2, sub.count(5, 3), delta);
    }
    @org.junit.Test
    public void ZeroXZeroTest() {
        double delta = 0.0000001;
        assertEquals(0, sub.count(0, 0), delta);
    }
    @org.junit.Test
    public void NumberXZeroTest() {
        double delta = 0.0000001;
        assertEquals(5, sub.count(5, 0), delta);
    }
    @org.junit.Test
    public void ZeroXNumberTest() {
        double delta = 0.0000001;
        assertEquals(-6, sub.count(0, 6), delta);
    }
}