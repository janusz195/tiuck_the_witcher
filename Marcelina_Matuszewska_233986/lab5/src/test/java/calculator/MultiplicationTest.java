package calculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class MultiplicationTest {
    Multiplication multi = new Multiplication();
    @org.junit.Test
    public void NormalMultiplicationTest() {
        double delta = 0.0000001;
        assertEquals(1, multi.count(1, 1), delta);
    }
    @org.junit.Test
    public void ZeroXZeroTest() {
        double delta = 0.0000001;
        assertEquals(0, multi.count(0, 0), delta);
    }
    @org.junit.Test
    public void NumberXZeroTest() {
        double delta = 0.0000001;
        assertEquals(0, multi.count(5, 0), delta);
    }
    @org.junit.Test
    public void ZeroXNumberTest() {
        double delta = 0.0000001;
        assertEquals(0, multi.count(0, 5), delta);
    }
}