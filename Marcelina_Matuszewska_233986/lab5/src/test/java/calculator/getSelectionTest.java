package calculator;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.*;

public class getSelectionTest {

    @Test
    public void setSelectionTest1() {
        getSelection getSel = new getSelection();
        final String sel = "1";
        InputStream stream = new ByteArrayInputStream(sel.getBytes());
        assertEquals(1, getSel.setSelection(stream));
    }
    @Test
    public void setSelectionTest2() {
        getSelection getSel = new getSelection();
        final String sel = "2";
        InputStream stream = new ByteArrayInputStream(sel.getBytes());
        assertEquals(2, getSel.setSelection(stream));
    }
    @Test
    public void setSelectionTest3() {
        getSelection getSel = new getSelection();
        final String sel = "3";
        InputStream stream = new ByteArrayInputStream(sel.getBytes());
        assertEquals(3, getSel.setSelection(stream));
    }
    @Test
    public void setSelectionTest4() {
        getSelection getSel = new getSelection();
        final String sel = "4";
        InputStream stream = new ByteArrayInputStream(sel.getBytes());
        assertEquals(4, getSel.setSelection(stream));
    }
}