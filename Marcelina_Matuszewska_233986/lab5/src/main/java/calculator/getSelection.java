package calculator;

import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Logger;

public class getSelection {
        private static Logger logger = Logger.getLogger("InfoLogging");
        int setSelection(InputStream inputStream) {
                int selection1;
                Scanner scanner = new Scanner(inputStream);
                logger.info("Make your selection and press enter: ");
                logger.info("1.Addition");
                logger.info("2.Subtraction");
                logger.info("3.Division");
                logger.info("4.Multiplication");
                selection1 = scanner.nextInt();
                while (selection1 != 1 && selection1 != 2 && selection1 != 3 && selection1 != 4) {
                        logger.info("Please enter correct number: ");
                        selection1 = scanner.nextInt();
                }
                return selection1;
        }
}
