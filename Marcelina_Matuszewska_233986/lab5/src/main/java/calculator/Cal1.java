package calculator;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Cal1 {
    private static Logger logger = Logger.getLogger("InfoLogging");

    public static void main(String[] args) {
        char wantToContinue;
        double userNumber1;
        double userNumber2;
        double answer1;
        int selection1;
        getValues getVal = new getValues();
        getSelection getSel = new getSelection();
        getMathOperations getMO = new getMathOperations();
        getNextOperation getNO = new getNextOperation();
        do {
            userNumber1 = getVal.setValues(System.in);
            userNumber2 = getVal.setValues(System.in);
            selection1 = getSel.setSelection(System.in);
            answer1 = getMO.mathOperations(selection1, userNumber1, userNumber2);
            String text1 = "Your answer is: {0}";
            logger.log(Level.INFO, text1, answer1);
            wantToContinue = getNO.askForNextOperation(System.in);
        } while (wantToContinue == 'y');
    }
}