package calculator;

import java.util.logging.Level;
import java.util.logging.Logger;

public class getMathOperations {
    private static Logger logger = Logger.getLogger("InfoLogging");
    double mathOperations(int selection, double userNumber1, double userNumber2) {
        Multiplication multi = new Multiplication();
        Division div = new Division();
        Addition add = new Addition();
        Subtraction sub = new Subtraction();
        double answer = 0;
        String text1 = "Your answer is: {0}";
        switch (selection) {
            case 1:
                answer = add.count(userNumber1, userNumber2);
                logger.log(Level.INFO, text1, answer);
                break;
            case 2:
                answer = sub.count(userNumber1, userNumber2);
                logger.log(Level.INFO, text1, answer);
                break;
            case 3:
                answer = div.count(userNumber1, userNumber2);
                logger.log(Level.INFO, text1, answer);
                break;
            case 4:
                answer = multi.count(userNumber1, userNumber2);
                logger.log(Level.INFO, text1, answer);
                break;
            default:
                break;
        }
        return answer;
    }
}
