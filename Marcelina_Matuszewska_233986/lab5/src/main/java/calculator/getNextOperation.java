package calculator;
import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Logger;
public class getNextOperation {
    private static Logger logger = Logger.getLogger("InfoLogging");
    public char askForNextOperation(InputStream inputStream) {
        char yesNo;
        logger.info("Do you want to calculate next operation? (y/n)");
        Scanner scanner1 = new Scanner(inputStream);
        yesNo = scanner1.next().charAt(0);
        return yesNo;
    }
}
