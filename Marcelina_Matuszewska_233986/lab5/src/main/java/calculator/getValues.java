package calculator;
import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Logger;
public class getValues {
    private static Logger logger = Logger.getLogger("InfoLogging");
    double setValues(InputStream inputStream) {
        double userNumber;
        logger.info("Please enter a number: ");
        Scanner scanner = new Scanner(inputStream);
        userNumber = scanner.nextDouble();
        return userNumber;
    }
}
