import gfdhfh.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void shouldReturnCorrectAdditionResult() {
        //given
        Calculator calculator = new Calculator();
        calculator.setNumberOne(12);
        calculator.setNumberTwo(13);
        Calculable operation = new Addition();
        //when
        final double result = calculator.calculate(operation);
        final double expectedValue = 25.0;
        //then
        assertEquals("Value is wrong",expectedValue,result,0);
    }

    @Test
    public void shouldReturnCorrectSubtractionResult() {
        //given
        Calculator calculator = new Calculator();
        calculator.setNumberOne(12);
        calculator.setNumberTwo(13);
        Calculable operation = new Substraction();
        //when
        final double result = calculator.calculate(operation);
        final double expectedValue =-1.0;
        //then
        assertEquals("Value is wrong",expectedValue,result,0);
    }

    @Test
    public void shouldReturnCorrectMultiplicationResult() {
        //given
        Calculator calculator = new Calculator();
        calculator.setNumberOne(11);
        calculator.setNumberTwo(11);
        Calculable operation = new Multiplication();
        //when
        final double result = calculator.calculate(operation);
        final double expectedValue = 121;
        //then
        assertEquals("Value is wrong",expectedValue,result,0);
    }

    @Test
    public void shouldReturnCorrectDivisionResult() {
        //given
        Calculator calculator = new Calculator();
        calculator.setNumberOne(11);
        calculator.setNumberTwo(11);
        Calculable operation = new Division();
        //when
        final double result = calculator.calculate(operation);
        final double expectedValue = 1;
        //then
        assertEquals("Value is wrong",expectedValue,result,0);
    }

    @Test
    public void shouldReturnNaNFromDivisionResult() {
        //given
        Calculator calculator = new Calculator();
        calculator.setNumberOne(11);
        calculator.setNumberTwo(0);
        Calculable operation = new Division();
        //when
        final double result = calculator.calculate(operation);
        //then
        assertTrue(Double.isNaN(result));
    }
}