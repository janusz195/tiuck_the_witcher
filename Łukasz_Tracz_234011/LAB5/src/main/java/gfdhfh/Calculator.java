package gfdhfh;

public class Calculator {

    private double numberOne = 0.0;
    private double numberTwo = 0.0;

    public double getNumberOne() {
        return numberOne;
    }

    public void setNumberOne(double numberOne) {
        this.numberOne = numberOne;
    }

    public double getNumberTwo() {
        return numberTwo;
    }

    public void setNumberTwo(double numberTwo) {
        this.numberTwo = numberTwo;
    }

    public boolean parse(Number number, String textToParseOnDouble) {
        double temporary;
        try {
            temporary = Double.parseDouble(textToParseOnDouble);
            if (number == Number.ONE) {
                numberOne = temporary;
                return true;
            } else {
                numberTwo = temporary;
                return true;
            }
        } catch (NumberFormatException e) {
            System.out.println("Podanego tekstu nie można sparsować na liczbę typu double");
        }
        return false;
    }


    public double calculate(Calculable operation) {
        return operation.calculate(numberOne, numberTwo);
    }


    public Calculable selectAction(int valueRepresentAction) {
        switch (valueRepresentAction) {
            case 1: {
                return new Addition();
            }
            case 2: {
                return new Substraction();
            }
            case 3: {
                return new Multiplication();
            }
            case 4: {
                return new Division();
            }
            default: {
                Printer.print("Wybrano nieznane działanie, wprowdź działanie ponownie");
                return new UndefinedOperation();
            }
        }
    }

    public String getSetOfOperations() {
        return "1 - Dodawanie\n2 - Odejmowanie\n3 - Mnożenie\n4 - Dzielenie";
    }

    public Calculable chooseOperation() {
        final String output = InputerWithCommand.inputWithCommand("Wybierz działanie wpisując odpowiednią cyfrę");
        try {
            return selectAction(Integer.parseInt(output));
        } catch (NumberFormatException e) {
            Printer.print("Wybrano nieznane działanie, wprowdź działanie ponownie");
            return new UndefinedOperation();
        }
    }

    public boolean areYouContinue(){
        String output = InputerWithCommand.inputWithCommand("Czy chcesz dalej liczyć TAK/NIE");
        switch (output){
            case "TAK": {
                return true;
            }
            case "NIE": {
                return false;
            }
            default:{
                Printer.print("Nie można sparsować wyniku operacji, uznano to za NIE");
                return false;
            }
        }
    }
}
