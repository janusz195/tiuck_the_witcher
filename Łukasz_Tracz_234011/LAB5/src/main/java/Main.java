import gfdhfh.*;
import gfdhfh.Number;

public class Main {

    public static void main(String[] args){

        Calculator calculator = new Calculator();
        runCalculator(calculator);
    }

    public static void runCalculator(Calculator calculator){
        boolean replay = false;
        boolean first = true;
        do {
            replay = false;
            if(first) {
                String numberOne = InputerWithCommand.inputWithCommand("Wprawadź pierwszą liczbę");
                replay = !calculator.parse(Number.ONE, numberOne);
                first =false;
            }
            Printer.print(calculator.getSetOfOperations());
            Calculable operation = calculator.chooseOperation();
            String numberTwo = InputerWithCommand.inputWithCommand("Wprawadź drugą liczbę");
            calculator.parse(Number.TWO, numberTwo);
            double result = calculator.calculate(operation);
            Printer.print(String.format("Wynik operacji: %s", result));
            calculator.parse(Number.ONE, String.valueOf(result));
            if(calculator.areYouContinue()){
                if(Double.isNaN(result)) {
                    calculator.setNumberOne(0);
                }else{
                    calculator.setNumberOne(result);
                }
                replay =true;
            }
        } while (replay);
    }
}
