const Texture = function () {

    /**
     *
     * @param gl
     * @param url
     * @returns {WebGLTexture}
     */
    const loadTexture = function (gl, url) {
        const texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);

        /*
         Until images are downloaded put a single pixel
         in the texture so it can be used immediately.
        */
        const level = 0;
        const internalFormat = gl.RGBA;
        const width = 1;
        const height = 1;
        const border = 0;
        const srcFormat = gl.RGBA;
        const srcType = gl.UNSIGNED_BYTE;
        const pixel = new Uint8Array([0, 0, 255, 255]);

        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
            width, height, border, srcFormat, srcType,
            pixel);

        const image = new Image();
        image.onload = function () {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                srcFormat, srcType, image);

            /*
             WebGL1 has different requirements for power of 2 images
             vs non power of 2 images so check if the image is a
             power of 2 in both dimensions.
            */
            if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
                gl.generateMipmap(gl.TEXTURE_2D);
            } else {
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            }
        };

        image.src = url;

        return texture;
    }
    return {
        loadTexture: loadTexture
    };
}

/**
 *
 * @param value
 * @returns {boolean}
 */
function isPowerOf2(value) {
    return (value & (value - 1)) == 0;
}